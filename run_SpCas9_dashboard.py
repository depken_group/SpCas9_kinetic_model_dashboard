import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objects as go 
import numpy as np 
import sys
import os
sys.path.append(os.getcwd())


# --- import code for kinetic model ----
from kinetic_model import active_Cas
from kinetic_model import dead_Cas
from kinetic_model import free_energy_landscape
from kinetic_model import CHAMP
from kinetic_model.CRISPRclass import CRISPR


# --- import code for components of the app ----
from app_components import graph_free_energy as graphFE
from app_components import graph_cleaved_fraction_vs_time as graphCLV
from app_components import graph_bound_fraction_vs_time as graphBND
from app_components import graph_binding_vs_concentration as graphKA
from app_components import user_entries as usrIN
from app_components import banner 
from app_components import user_tab as usrTab
from app_components import description_tab 

#########################
#   load parameter     ##
#########################
Cas9 = CRISPR(guide_length=20,
                  PAM_length=3,
                  PAM_sequence='NGG')
epsilon = np.loadtxt('parameters/SpCas9_epsilon.txt')
forward_rates = np.loadtxt('parameters/SpCas9_forward_rates.txt')


#########################
#   app components     ##
#########################

EntryGruide = usrIN.entry_guide
EntryTarget = usrIN.entry_target
ReactionTime_1 = usrIN.dropdown_TimeUnits
ReactionTime_2 = usrIN.entry_time
Concentration = usrIN.entry_concentration

MismatchPattern = html.P(id='display-mismatch-pattern')
FreeEnergyLandscape = dcc.Graph(id='free-energy-landscape')
CleavageReaction = dcc.Graph(id='clv-VS-time')
BindingReaction = dcc.Graph(id='bnd-VS-time')
BindingCurve  = dcc.Graph(id='bnd-VS-concentration')

Banner = banner.Banner


# --- create the two tabs for left hand side pannel--- 
UserInputTab = dcc.Tab(label="user entries",
    children=usrTab.create_tab())


descriptionTXT = description_tab.read_description()

DescriptionTab = dcc.Tab(label="about", 
    children=[html.Div(id='model-description', 
      children=[html.H4('Welcome'),
      descriptionTXT])])





#########################
#   Dash app           ##
#########################

app = dash.Dash()

app.layout = html.Div([ 
    Banner,
    html.Div(className='app-body',
      children=[
    #---- the user inputs are in control tab to the left ----
    html.Div(className='control-tabs', children=[
      dcc.Tabs([DescriptionTab, UserInputTab],
        colors={
        "border": '#d9d9d9',
        "primary": "#3690c0",
        "background": "white"
    }),
      ]),

    # --- graphs are to the right ----- 
    html.Div(className='graph-container',
             children=[

      # ---- display mismatch pattern on top ----

      html.Div(className='display-mismatch-pattern',
        children=["Mismatch pattern: ",MismatchPattern])
      ,
      html.Div([FreeEnergyLandscape]),
      html.Div(style={'columnCount':3},children=[
      CleavageReaction,
      BindingReaction,
      BindingCurve])
      ])
    ])
    ])



#########################
#   interactivity      ##
#########################
def reaction_time(value, units):
	if units == 's':
		multiply_by = 1.0
	elif units == 'min':
		multiply_by = 60.0
	elif units == 'hrs':
		multiply_by = 60. * 60.
	time_in_seconds = value * multiply_by
	return time_in_seconds 

@app.callback(
    dash.dependencies.Output('display-mismatch-pattern', 'children'),
    [dash.dependencies.Input('entry-guide', 'value'),
     dash.dependencies.Input('entry-target', 'value')])
def display_mismatch_pattern(guide, target):
    mm_pattern = np.array(['_'] * 21)

    if (len(guide) == 23) and (len(target) == 23):
        mismatch_positions = active_Cas.get_mismatch_positions(guide, target, Cas9)
        mm_pattern[mismatch_positions] = 'X'

    return "PAM +" + '|'+ '|'.join(mm_pattern) + '|'

@app.callback(
    dash.dependencies.Output('free-energy-landscape', 'figure'),
    [dash.dependencies.Input('entry-guide', 'value'),
     dash.dependencies.Input('entry-target', 'value'),
     dash.dependencies.Input('concentration-value','value')])
def update_FreeEnergyLandscape(guide,target,concentration):
	return graphFE.create_graph(guide,target,concentration,epsilon, Cas9)


@app.callback(
    dash.dependencies.Output('clv-VS-time', 'figure'),
    [dash.dependencies.Input('entry-guide', 'value'),
     dash.dependencies.Input('entry-target', 'value'),
     dash.dependencies.Input('concentration-value','value'),
     dash.dependencies.Input('reaction-time-value','value'),
     dash.dependencies.Input('reaction-time-unit','value')])
def update_CleavageReaction(guide,target,concentration,time_val, time_unit):
  time_of_reaction = reaction_time(time_val, time_unit)
  return graphCLV.create_graph(guide,target,concentration,time_of_reaction,epsilon,forward_rates, Cas9)



@app.callback(
    dash.dependencies.Output('bnd-VS-time', 'figure'),
    [dash.dependencies.Input('entry-guide', 'value'),
     dash.dependencies.Input('entry-target', 'value'),
     dash.dependencies.Input('concentration-value','value'),
     dash.dependencies.Input('reaction-time-value','value'),
     dash.dependencies.Input('reaction-time-unit','value')])
def update_BindingReaction(guide,target,concentration,time_val, time_unit):
  time_of_reaction = reaction_time(time_val, time_unit)
  return graphBND.create_graph(guide,target,concentration,time_of_reaction,epsilon,forward_rates, Cas9)


@app.callback(
    dash.dependencies.Output('bnd-VS-concentration', 'figure'),
    [dash.dependencies.Input('entry-guide', 'value'),
     dash.dependencies.Input('entry-target', 'value'),
     dash.dependencies.Input('reaction-time-value','value'),
     dash.dependencies.Input('reaction-time-unit','value')])
def update_BindingCurve(guide,target, time_val, time_unit):
	time_of_reaction = reaction_time(time_val, time_unit)
	return graphKA.create_graph(guide,target,time_of_reaction,epsilon,forward_rates, Cas9)

#########################
#   run app            ##
#########################

if __name__ == '__main__':
    app.run_server(debug=False)