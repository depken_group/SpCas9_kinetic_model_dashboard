import dash
import dash_core_components as dcc
import dash_html_components as html

from app_components import user_entries as usrIN



EntryGuide = usrIN.entry_guide
EntryTarget = usrIN.entry_target
ReactionTime_1 = usrIN.dropdown_TimeUnits
ReactionTime_2 = usrIN.entry_time
Concentration = usrIN.entry_concentration


text1 = dcc.Markdown("Guide sequence: ")
text2 = dcc.Markdown("Target sequence: ")
text3 = dcc.Markdown("reaction time:  ")
text4 = dcc.Markdown("(d)Cas9-sgRNA concentration in nM: ")

def create_tab():

	entry_guide = html.Div(
            title='Enter the nucleotide sequence of your on-target',
            className='app-controls-block',
            children=[
                html.Div(className='fullwidth-app-controls-name',
                         children='on-target sequence'),
                html.Div(
                    className='app-controls-desc',
                    children='Specify the on-target sequence as protoscpacer + PAM (3\'NGG5\') '
                ),
                EntryGuide
            ])



	entry_target = html.Div(
            title='Enter the nucleotide sequence of your off-target',
            className='app-controls-block',
            children=[
                html.Div(className='fullwidth-app-controls-name',
                         children='off-target sequence'),
                html.Div(
                    className='app-controls-desc',
                    children='Specify the off-target sequence as protoscpacer + PAM (3\'NGG5\') '
                ),
                EntryTarget
            ])


	entry_time_units = html.Div(
            title='Time units',
            className='app-controls-block',
            children=[
                html.Div(className='fullwidth-app-controls-name',
                         children='unit of time'),
                html.Div(
                    className='app-controls-desc',
                    children=''
                ),
                ReactionTime_1
            ])


	entry_reaction_time = html.Div(
            title='Time of reaction',
            className='app-controls-block',
            children=[
                html.Div(className='fullwidth-app-controls-name',
                         children='Time of exposure to Sp-(d)Cas9-sgRNA'),
                html.Div(
                    className='app-controls-desc',
                    children=''
                ),
                ReactionTime_1,
                ReactionTime_2
            ])


	entry_concentration = html.Div(
            title='CAs9-sgRNA concentration',
            className='app-controls-block',
            children=[
                html.Div(className='fullwidth-app-controls-name',
                         children='Concentration of Sp-(d)Cas9-sgRNA in nM'),
                html.Div(
                    className='app-controls-desc',
                    children=''
                ),
                Concentration

            ])








	return html.Div(className='control-tab',
		children=[
		entry_guide, 
		entry_target, 
		entry_reaction_time,
		entry_concentration]
		)



