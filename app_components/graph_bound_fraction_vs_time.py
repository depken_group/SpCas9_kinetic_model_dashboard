import numpy as np 
import plotly.graph_objects as go 
from kinetic_model import dead_Cas
from kinetic_model.change_concentration import change_concentration


def create_graph(guide, target, concentration, exposure_time, epsilon,forward_rates,Cas):
	
	# --- some fixed set of timepoints  --- 
	timepoints = np.array([0.0, 12.0, 60.0, 180.0, 600.0, 1800.0, 6000.0, 18000.0, 60000.0])

	# --- go to user-defined concentration --- 
	epsilon, forward_rates = change_concentration(epsilon_1nM=epsilon, 
		forward_rates_1nM=forward_rates, 
		new_concentration=concentration, 
		ref_concentration=1.0)


	# ---- perform calculation ---- 
	if((len(guide)==23) and (len(target)==23)):
		prob_bnd = dead_Cas.calc_fraction_bound(guide, target, epsilon, forward_rates,Cas, timepoints)
	else:
		prob_bnd = np.zeros(len(timepoints))



	#---- add dashed line at given exposure time --- 
	shapeDict = {
	"type":"line",
	'yref':'paper', 'y0':0, 'y1':1.,
	'xref':'x', 'x0':exposure_time, 'x1':exposure_time,
	'line':dict(color='#fdb462', width=3,dash='dash'),
	'opacity':1.,
	'layer':"below"
	}


    # --- create graph object ---- 
	bnd_vs_time = go.Scatter(x=timepoints , y=prob_bnd, mode='lines', marker={'color':'#fdb462'})
	layout = {}
	layout['title'] = "<b>bound fraction over time</b>"
	layout['xaxis'] = {'title':'time (s)', 'type':'log'}
	layout['yaxis'] = {'title':'fraction bound DNA'}
	layout['template'] = 'simple_white'
	layout['width'] = 333
	layout['height'] = 250
	layout['paper_bgcolor'] = '#f7fbff'
	layout['shapes'] = [shapeDict]

	fig = go.Figure(data=[bnd_vs_time],
     	             layout=layout)
	return fig 

	