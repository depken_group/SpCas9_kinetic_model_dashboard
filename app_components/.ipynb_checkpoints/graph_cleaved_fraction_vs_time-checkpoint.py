import numpy as np 
import plotly.graph_objects as go 
from kinetic_model import active_Cas
from kinetic_model.change_concentration import change_concentration


def create_graph(guide, target, concentration, exposure_time, epsilon,forward_rates,Cas):
	
	# --- some fixed set of timepoints  --- 
	timepoints = np.array([0.0, 12.0, 60.0, 180.0, 600.0, 1800.0, 6000.0, 18000.0, 60000.0])

	# --- go to user-defined concentration --- 
	epsilon, forward_rates = change_concentration(epsilon_1nM=epsilon, 
		forward_rates_1nM=forward_rates, 
		new_concentration=concentration, 
		ref_concentration=1.0)


	# ---- perform calculation ---- 
	if((len(guide)==23) and (len(target)==23)):
		prob_no_clv = active_Cas.calc_fraction_cleaved(guide, target, epsilon, forward_rates,Cas, timepoints)
	else:
		prob_no_clv = np.zeros(len(timepoints))


	# --- add line at given exposure time --- 
	shapeDict = {
	"type":"line",
	'yref':'paper', 'y0':0, 'y1':1.,
	'xref':'x', 'x0':exposure_time, 'x1':exposure_time,
	'line':dict(color='#ed174e', width=3,dash='dash'),
	'opacity':1.,
	'layer':"below"
	}


    # --- create graph object ---- 
	clv_vs_time = go.Scatter(x=timepoints , y=prob_no_clv, mode='lines', marker={'color':'#ed174e'})
	layout = {}
	layout['title'] = "<b>cut fraction DNA over time</b>"
	layout['xaxis'] = {'title':'time (s)', 'type':'log'}
	layout['yaxis'] = {'title':'fraction cut DNA'}
	layout['template'] = 'simple_white'
	layout['width'] = 333
	layout['height'] = 250
	layout['paper_bgcolor'] = '#f7fbff'
	layout['shapes'] = [shapeDict]

	fig = go.Figure(data=[clv_vs_time],
     	             layout=layout)
	return fig 

