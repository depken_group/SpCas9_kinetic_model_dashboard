import dash
import dash_core_components as dcc
import dash_html_components as html


def read_description():
	text_markdown = '''This dashboard complements the manuscript:
    
**A kinetic model predicts SpCas9 activity, improves off-target classification, and reveals the physical basis of targeting fidelity**. 

Under the *user entries* tab above you can enter both the **guide** and **target** sequence as 20-mer + (NGG) PAM (only the canonical PAM is currently implemented), the **concentration** of *Sp*-(d)Cas9-sgRNA, and the **exposure time** of the complex to the target.

To the right you will see the resulting graphs describing the activity on the target you chose. The panels are
* **Mismatch pattern**: The current model parameterisation is sequence averaged, and ‘_’/‘X’ denotes a match/mismatch position for the guide and target sequence set by the user.
* The **free-energy landscape** for interactions with the on-target (pink) and off-target (green) at the concentration set by the user. Mismatch positions are indicated with dashed lines. The states on the x-axis correspond to:
    * -1: solution state
    * 0: PAM bound
    * 1-20: R1-R20
* The fraction of **cut DNA** (*Sp*Cas9) over time, when target is exposed at the concentration set by user. Dashed line denotes exposure time entered by user. 
* The fraction of **bound DNA** (*Sp*-dCas9) over time, when target is exposed at the concentration set by user. Dashed line denotes exposure time entered by user. 
* The fraction **bound DNA** (*Sp*-dCas9) as a function of concentration, when target is exposed for the exposure time entered byt the user (on-target:solid pink, chosen target: solid green). Dashed curves are the equilibrium binding curves.  
	'''

	return dcc.Markdown(text_markdown)








