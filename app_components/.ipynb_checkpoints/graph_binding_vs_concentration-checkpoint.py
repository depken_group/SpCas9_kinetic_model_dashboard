import numpy as np 
import plotly.graph_objects as go 
from kinetic_model import CHAMP
from kinetic_model import dead_Cas 


def create_graph(guide, target, time, epsilon, forward_rates, Cas):
	# --- some fixed set of concentrations  ---
	concentrations = np.array([0.1,0.3,1.,3.,10.,30.,100.,300.])

	# ---- perform calculation ---- 
	if( (len(guide)==23) and (len(target)==23) ):
		_,Pbound_OnTarget,_ = CHAMP.binding_curve(guide, guide, epsilon, forward_rates, concentrations, Cas, time)
		ProbEQ_OnTarget = dead_Cas.equillibrium_binding_curve(guide, guide,concentrations, epsilon, forward_rates, Cas)

		_,Pbound_OffTarget,_ = CHAMP.binding_curve(guide, target, epsilon, forward_rates, concentrations, Cas, time)
		ProbEQ_OffTarget = dead_Cas.equillibrium_binding_curve(guide, target,concentrations, epsilon, forward_rates, Cas)

	else:
		Pbound_OnTarget = np.zeros(len(concentrations))
		ProbEQ_OnTarget = np.zeros(len(concentrations))
		Pbound_OffTarget = np.zeros(len(concentrations))
		ProbEQ_OffTarget = np.zeros(len(concentrations))




    	
    # --- create graph object ---- 
	bnd_curve_OT = go.Scatter(x=concentrations , y=Pbound_OnTarget,
							  mode='lines',
							  line=dict(color='#e78ac3',dash='solid'),
							  name='on-target at time: ' + str(time) + 's')
	eq_curve_OT  = go.Scatter(x=concentrations , y=ProbEQ_OnTarget,
							  mode='lines',
							  line=dict(color='#e78ac3', dash='dash'),
							  name='on-target equillibrated')

	bnd_curve = go.Scatter(x=concentrations , y=Pbound_OffTarget,
						   mode='lines',
						   line=dict(color='#66c2a5', dash='solid'),
						   name='off-target at time: ' + str(time) + 's')
	eq_curve  = go.Scatter(x=concentrations , y=ProbEQ_OffTarget,
						   mode='lines',
						   line=dict(color='#66c2a5', dash='dash'),
						   name='off-target equillibrated')

	layout = {}
	layout['title'] = "<b>binding curve</b>"
	layout['xaxis'] = {'title':'concentration of dCas9-sgRNA (nM)', 'type':'log'}
	layout['yaxis'] = {'title':'fraction bound DNA'}
	layout['template'] = 'simple_white'
	layout['width'] = 333
	layout['height'] = 250
	layout['showlegend'] = False
	layout['paper_bgcolor'] = '#f7fbff'
	fig = go.Figure(data=[bnd_curve_OT, eq_curve_OT, bnd_curve, eq_curve],
		layout=layout) 
	return fig 



