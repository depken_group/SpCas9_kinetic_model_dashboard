import numpy as np 
from kinetic_model import free_energy_landscape
from kinetic_model import active_Cas
import plotly.graph_objects as go



def create_graph(guide,target,concentration,epsilon, Cas):
	# ---- perform calculation ---- 
    if (len(guide)==23) and (len(target)==23):
        on_target = free_energy_landscape.plot_landscape(guide, guide, epsilon, Cas, rel_concentration=concentration,show_plot=False)
        off_target= free_energy_landscape.plot_landscape(guide, target, epsilon, Cas,rel_concentration=concentration, show_plot=False)

        mismatch_positions = active_Cas.get_mismatch_positions(guide, target, Cas)
    else:
        on_target = np.zeros(22)
        off_target= np.zeros(22)
        mismatch_positions = []
    


    # --- create list of vertical lines at mismatch positions --- 
    vert_lines = []
    for mm_pos in mismatch_positions:
        shapeDict = {
        'type':"line",
        'yref':'paper', 'y0':0, 'y1':1,
        'xref':'x', 'x0':mm_pos, 'x1':mm_pos,
        'line':dict(color="IndianRed", width=3, dash='dash'),
        'opacity': 1.,
        'layer' : "below"
        }

        vert_lines.append( shapeDict)

    # --- create graph object ---- 
    graph_on_target = go.Scatter(x=list(range(-1,21)) , y=on_target,
                                 mode='lines+markers',
                                 marker={'color':'#e78ac3'},
                                 name='on-target')
    graph_off_target = go.Scatter(x=list(range(-1,21)) , y=off_target,
                                  mode='lines+markers',
                                  marker={'color':'#66c2a5'},   #92c6df
                                  name='off-target')
    layout = {}
    layout['title'] = "<b>Free-Energy Landscape</b>"  #+ '\t'*4 + display_mismatch_pattern(guide,target,Cas)
    layout['xaxis'] = {'title':'targeting progression'}
    layout['yaxis'] = {'title':'free-energy (kT)'}
    layout['template'] = 'simple_white'
    layout['shapes'] = vert_lines
    layout['width'] = 800
    layout['height'] = 400
    layout['paper_bgcolor'] = '#f7fbff'
    layout['legend'] = dict(x=0.847, y=1.2, bgcolor="white")

    fig = go.Figure(data=[graph_off_target, graph_on_target],
     	             layout=layout)
    return fig




def display_mismatch_pattern(guide, target,Cas):
    mm_pattern = np.array(['_'] * 23)

    if (len(guide) == 23) and (len(target) == 23):
        mismatch_positions = active_Cas.get_mismatch_positions(guide, target, Cas)
        mm_pattern[mismatch_positions] = 'X'

    return "Mismatch pattern: " + "PAM +" + '|'.join(mm_pattern)


