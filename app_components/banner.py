import dash
import dash_core_components as dcc
import dash_html_components as html

banner_title = html.H1("Sp-(d)Cas9 kinetic activity predictor")
img = html.Img(src='assets/Cas.png')

Banner = html.Div( children=[banner_title, img], id='banner', className='banner')
