
import dash
import dash_core_components as dcc
import dash_html_components as html


entry_guide = dcc.Input(
    id='entry-guide',
    placeholder='Enter your guide sequence...',
    type='text',
    value='GACGCATAAAGATGAGACGCTGG'
)

entry_target = dcc.Input(
    id='entry-target',
    placeholder='Enter your target sequence...',
    type='text',
    value='GACGCATAAAGATGAGACGCTGG',
)



dropdown_TimeUnits = dcc.Dropdown(id='reaction-time-unit',
	options=[
	{'label':'seconds', 'value':'s'},
	{'label':'minutes', 'value':'min'},
	{'label':'hours', 'value':'hrs'}
	],
	value='hrs'
	)

entry_time = dcc.Input(id='reaction-time-value',
	placeholder='Enter a value...',
	type='number',
	min=0.001,
	max=10**6,
	value = 1.0,
	debounce=True
	)


entry_concentration = dcc.Input(id='concentration-value',
	placeholder='Enter concentration in nM....',
	type='number',
	min=0.0,
	max=10**9,
	step=1.0,
	value=10.0,
	debounce=True
	)
# slider_concentration = dcc.Slider()

# slider_reactionTime = dcc.Slider()



