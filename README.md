# Overview
Here we provide the core modules to calculate both binding and cleavage activity of _Sp_(d)-Cas9 based on kinetic modelling and training on the CHAMP and NucleaSeq data as described in the manuscript _"A kinetic model predicts SpCas9 activity, improves off-target classification, and reveals the physical basis of targeting fidelity"_ ([preprint](https://www.biorxiv.org/content/10.1101/2020.05.21.108613v2)).

We here describe how to use our dashboard app and functions to predict the binding/cleavage activity of (d)_Sp_Cas9 for a given guide and target sequence. 

# System requirement
## Hardware requirements
Package should run on any standard computer
## Software requirements
## OS requirments
This package has been tested on Windows 10, but should run under `conda` in any operating system
## Python requirments
`python=3.6 
scipy=1.3.1 
pandas==0.25.1 
numpy==1.17.2 
seaborn==0.9.0 
plotly==4.6.0 
matplotlib==3.1.1`
# Installation guide

## Clone the directory
Make a local clone of this repository, and working in a conda terminal ([conda](https://conda.io/projects/conda/en/latest/index.html)) navigate to the root of the cloned directory

## Create a conda environment KinPred with the required python packages.
```
conda create -n KinPred python=3.6 scipy=1.3.1 pandas==0.25.1 numpy==1.17.2 seaborn==0.9.0 plotly==4.6.0 matplotlib==3.1.1
```

## Activate conda environement KinPred 
```
conda activate KinPred
```
## Install additional required packages via PIP
```
pip install -r requirements.txt
```

# Run the dashboard app
```
python run_SpCas9_dashboard.py
```
you will see the following appear:
```
Running on http://127.0.0.1:8050/ (Press CTRL+C to quit)
```
Copy the link displayed into your browser. 

Once you have loaded the dashboard into your browser, you will see a `about` tab explaining the imput that should be entered in the `user entry` tab

# Procedures to calculate activity 
Inside the `kinetic_model/`-deriectory you can find all functions to compute the kinetic properties of a given guided nuclease system. The nuclease system is defined byt a set of 20 on-target free energy costs (\epsilon_n) followed by 20 free-energy penalties (\delta\epsilon_n) (refered to collectively as `epsilon`) as well as four rates (kon, kPAM, kf, kclv) (refered to collectively as `forward_rates`) that completely parameterize the microscopic system. 

## The CRISPR class
Here we define the properties of the guided nuclease system at hand. 
```python
from model.CRISPRclass import CRISPR 
Cas9 = CRISPR(guide_length=20,
              PAM_length=3,
              PAM_sequence='NGG')

```
All functions within `kinetic_model/` take `epsilon`, `forward_rates` and an instance of the `CRISPR`-class as input. 

## Loading in a dataset 
All functions are written such that they take in a guide sequence, a target sequence, `epsilon`, and `forward_rates`.

The parameter set corresponding to the best-fit obtained for SpCas9 (see pink values in Figure 3 of manuscript) are stored in the `parameters/` directory. 

To load them into python

```python 
import numpy 
epsilon = np.loadtxt('parameters/SpCas9_epsilon.txt')
forward_rates = np.loadtxt('parameters/SpCas9_forward_rates.txt')

```

By default parameters corresponding to 1nM enzyme concentration will be returned. If you want to manually change the concentration:
```python 
from kinetic_model.change_concentration import * 
epsilon_new, forward_rates_new = change_concentration(epsilon_1nM=epsilon,
                                                      forward_rates_1nM=forward_rates,
                                                      new_concentration=concentration)
```


## Calculate the free-energy landscapes 
Once you've defined the instance of `CRISPRclass.CRISPR()` and loaded (or defined) the `epsilon` and `forward_rates` arrays, we can now simply evaluate all kinetic or equillibrium properties we want.
For instace, to obtain the free-energy landscape (**Figures 3 and 4 of the manuscript**). 
```python
from kinetic_model import free_energy_landscape 
landscape = free_energy_landscape.plot_landscape(guide=targetE,
                                                 target=targetE,
                                                 epsilon=epsilon, 
                                                 Cas=Cas9,show_plot=False);

```

## Compute kinetic properties 
Similarly, we get the bound fraction over time for dCas9 using, 
```python
from kinetic_model import active_cas
timepoints = np.array([0.0, 12.0, 60.0, 180.0, 600.0, 1800.0, 6000.0, 18000.0, 60000.0])
ProbCLV = active_Cas.calc_fraction_cleaved(guide, target epsilon, forward_rates, 
                                           Cas=Cas9, 
                                           timepoints=timepoints)

```
, or the cleaved fraction over time  using
```python
from kinetic_model import dead_cas
timepoints = np.array([0.0, 12.0, 60.0, 180.0, 600.0, 1800.0, 6000.0, 18000.0, 60000.0])
ProbBND = dead_Cas.calc_fraction_bound(guide, target epsilon, forward_rates, 
                                       Cas=Cas9, 
                                       timepoints=timepoints)


```
To predict the outcome of either CHAMP, NucleaSeq or HiTS-FLIP experiments (__as done for Figure 1 of the manuscript__), use functions in their respective modules
```python
# --- effective cleavage rate as measured in NucleaSeq experiment ---- 
from kinetic_model import NucleaSeq
kclv = NucleaSeq.calc_cleavage_rate(guide,target,epsilon,forward_rates, 
                                    Cas=Cas9)



#--- effective association constant as measured in CHAMP experiment --- 
# NOTE: CHAMP returns the logarithm of dissociation constant. 
from kinetic_model import CHAMP 
import numpy as np 
ABA = CHAMP.calc_ABA(guide, target, epsilon, forward_rates, 
                     Cas=Cas9)
Ka  = np.exp(-1 * ABA)


#--- effective association rate as measured in HiTS-FLIP experiment --- 
from kinetic_model import HiTS_FLIP 
ka = HiTS_FLIP.calc_association_rate(guide, target, epsilon, forward_rates, 
                                     Cas=Cas9)
```

To calculate coarse-grained parameter values from the microscopic parameter values (__Figure 3 of the manuscript__) one uses, 
```python
from kinetic_model import active_cas
cg_energies, cg_fwd_rates, cg_bck_rates = active_cas.cg_HNH(guide, target, epsilon,forward_rates) 
```
, which returns the free-energies of the coarse-grained states as well as the forward and backward transition rates between them. 
For completeness sake the unbound state and post-cleavage state are also included. 

State occupation probabilities for dCas9 at time T are calculated using 
```python
from kinetic_model import active_cas
Probabilities = dead_Cas.get_Probability(rate_matrix, initial_condition=np.array([1.0]+[0.0]*21),T=T)
```


The cleavage probability once the Cas9-sgRNA has bound the PAM - the quantity used 
in __Figure 6 of the mansucript__ for genome-wide off-target prediction and is decribed in [_Hybridization Kinetics Explains CRISPR-Cas OffTargeting Rules_](https://www.cell.com/cell-reports/references/S2211-1247(18)30077-9) - can be computed using 
```python
from kinetic_model import active_cas
Prob = active_Cas.Pclv(guide, target, epsilon,forward_rates,Cas)
```







